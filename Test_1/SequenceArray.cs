﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_1
{
    class SequenceArray
    {
        int Rows, Columns;
        char[,] mass;
        bool[,] access;
        enum direction { right = '-', down_right = '\\', down = '|', down_left = '/' };

        public SequenceArray(int rows = 0, int columns = 0)
        {
            Rows = rows;
            Columns = columns;
            mass = new char[Rows, Columns];
            mass.Initialize();
            access = new bool[Rows, Columns];
            accessFillTrue();
        }
        void accessFillTrue()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Columns; j++)
                    access[i, j] = true;
        }
        public void setMass(char[,] inputMass)
        {
            if (inputMass.GetLength(0) == Rows && inputMass.GetLength(1) == Columns)
                for (int i = 0; i < Rows; i++)
                    for (int j = 0; j < Columns; j++)
                        mass[i, j] = inputMass[i, j];

            else
                throw new System.IndexOutOfRangeException("Invalid Data");
        }
        public string findSequences()
        {
            StringBuilder report = new StringBuilder();
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    report.Append(getReportSequences(i, j, direction.right));
                    report.Append(getReportSequences(i, j, direction.down_right));
                    report.Append(getReportSequences(i, j, direction.down));
                    report.Append(getReportSequences(i, j, direction.down_left));
                }
            }
            return report.ToString();
        }
        string getReportSequences(int indexRow, int indexColumn, direction type)
        {
            string res = "";
            if (access[indexRow, indexColumn])
            {
                int count;
                count = countElementSequence(indexRow, indexColumn, type);
                if (count > 1)
                {
                    res += (char)type;
                    res += " [" + (indexRow + 1) + " " + (indexColumn + 1) + "] ";
                    res += mass[indexRow, indexColumn] + " " + count + "\n";
                }
                return res;
            }
            else
                return res;
        }
        int countElementSequence(int indexRow, int indexColumn, direction type)
        {
            int count = 1;
            int Istep, Jstep, i, j;
            Istep = getIStep(type);
            Jstep = getJStep(type);
            i = indexRow;
            j = indexColumn;

            while (true)
            {

                if (i + Istep < Rows && j + Jstep < Columns && j + Jstep >= 0) //если следующий элемент существует
                {
                    if (mass[i, j] == mass[i + Istep, j + Jstep]) // если он похож на предыидущий 
                    {
                        count++;
                        i += Istep;
                        j += Jstep;
                        access[i, j] = false; //отмечаем посещённые адреса
                    }
                    else
                        break;
                }
                else
                    break;
            }
            return count;
        }
        int getIStep(direction type)
        {
            switch (type)
            {
                case direction.right:
                    return 0;
                case direction.down_right:
                    return 1;
                case direction.down:
                    return 1;
                case direction.down_left:
                    return 1;
                default:
                    return 0;
            }
        }
        int getJStep(direction type)
        {
            switch (type)
            {
                case direction.right:
                    return 1;
                case direction.down_right:
                    return 1;
                case direction.down:
                    return 0;
                case direction.down_left:
                    return -1;
                default:
                    return 0;
            }
        }
        public override string ToString()
        {
            string res = "";
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                    res += mass[i, j] + " ";
                res += "\n";
            }
            return res;
        }
    }
}
