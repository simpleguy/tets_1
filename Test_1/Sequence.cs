﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_1
{
    class Sequence
    {
        SequenceArray array;

        public void EnterData()
        {
            try
            {
                int row, column;
                char[,] tempArray;
                Console.Write("Количество строк M = ");
                row = Input.getInt();

                Console.Write("Количество столбцов N = ");
                column = Input.getInt();

                Console.WriteLine("Введите массив:");
                tempArray = Input.getMass(row, column);

                array = new SequenceArray(row, column);
                array.setMass(tempArray);
            }
            catch (FormatException e)
            {
                array = new SequenceArray();
                Console.WriteLine(e.Message);
            }
        }
        public string GetResult()
        {
            return array.findSequences();
        }

        public override string ToString()
        {
            return array.ToString();
        }
    }
}
