﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_1
{
    class Input
    {
        public static int getInt()
        {
            int res = 0;
            string line;
            line = Console.ReadLine();
            if (!int.TryParse(line, out res))
            {
                throw new System.FormatException("Invalid value");
            }
            return res;
        }
        public static char[,] getMass(int rows, int columns)
        {
            char[,] res = new char[rows, columns];
            string line;
            char[] charArray;
            for (int i = 0; i < rows; i++)
            {
                line = Console.ReadLine();
                charArray = parseLineBySymbol(line, columns);
                for (int j = 0; j < columns; j++)
                    res[i, j] = charArray[j];
            }
            return res;
        }
        static char[] parseLineBySymbol(string line, int count)
        {
            char[] res = new char[count];
            string[] strArray = line.Split(' ');

            if (strArray.Length != count)
                throw new System.FormatException("Invalid number of items");

            for (int i = 0; i < count; i++)
            {
                if (strArray[i].Length == 1)
                    res[i] = strArray[i][0]; // берё символ из строки
                else
                    throw new System.FormatException("Invalid " + i + "item");
            }
            return res;
        }
    }
}
